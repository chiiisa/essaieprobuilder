﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Target : MonoBehaviour
{
    public float health = 50f;


    public void Damage(float Degat)
    {
        health -= Degat;

        if (health <= 0)
        {
            Destroy(gameObject);
            GameObject.Find("flaregun").GetComponent<GunManager>().Ennemytué++;
        }
    }
}
