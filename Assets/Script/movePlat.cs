﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movePlat : MonoBehaviour
{
    Vector3 pos;
    private float temps = 1f;
    private float envergure = 2.7f;


    void Start () {
        pos = transform.position;
    }
	
	
	void Update () {
        transform.position = new Vector3(pos.x, pos.y + Mathf.PingPong(Time.time / temps, envergure), pos.z);
    }

}
