﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveTarget : MonoBehaviour
{
    private Vector3 direction;
    private float speed = 0.5f;


     private float x;
     private float z;

     public float xMax;
     public float zMax;
     public float xMin;
     public float zMin;

    // Start is called before the first frame update
    void Start()
    {
        direction = (new Vector3(Random.Range(-1.0f, 1.0f), 0.0f, Random.Range(-1.0f, 1.0f))).normalized;
    }

    // Update is called once per frame
    void Update()
    {
        if(transform.position.x > xMax)
        {
            x = Random.Range(-1.0f,0.0f);
            z = Random.Range(-1.0f,1.0f);
            direction = (new Vector3(x,0,z));
        }

        if (transform.position.x < xMin)
        {
            x = Random.Range(0.0f,1.0f);
            z = Random.Range(-1.0f,1.0f);
            direction = (new Vector3(x,0,z));
        }

        if (transform.position.z > zMax)
        {
            x = Random.Range(-1.0f,1.0f);
            z = Random.Range(-1.0f,0.0f);
            direction = (new Vector3(x,0,z));
        }
        if (transform.position.z < zMin)
        {
            z = Random.Range(0.0f,1.0f);
            x = Random.Range(-1.0f,1.0f);
            direction = (new Vector3(x,0,z));
        }
        transform.position += direction * speed * Time.deltaTime;
    }



}
