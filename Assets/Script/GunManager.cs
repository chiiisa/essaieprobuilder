﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GunManager : MonoBehaviour
{
    private Vector3 distance;

    [SerializeField] private GameObject muni;
    [SerializeField] private Transform point;
    public Rigidbody bullet;

    [SerializeField] private Text Score;
    public int Ennemytué = 0;

    private int damage = 25;


    void Start()
    {
        Score.text = "Ennemy tué : " + Ennemytué;
    }

    // Update is called once per frame
    void Update()
    {
        distance = Input.mousePosition;
        distance.z = 1.40f;
        transform.position = Camera.main.ScreenToWorldPoint(distance);

        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("tire");
            //Instantiate(muni,point.transform.position,Quaternion.identity);
            shoot();
        }

        Score.text = "Ennemy tué : " + Ennemytué;

    }

    void shoot()
    {

        /*Rigidbody bulletClone = (Rigidbody)Instantiate(bullet, point.transform.position, transform.rotation);
        bulletClone.velocity = point.transform.forward * 100;*/

        RaycastHit hit;
        if (Physics.Raycast(point.transform.position, point.transform.forward, out hit,200f))
        {
            Debug.Log(hit.transform.name);
            if (hit.transform.tag == "enemy")
            {
                Target target = hit.transform.GetComponent<Target>();
                target.Damage(damage);
            }
        }

    }
}
